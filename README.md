# Online Admission System 
## _For college program applications_

Online Admission System is a programs applications system for a college or other educational institute. The students can register and choose proper programs after login. students can update their own profiles and admissions  
before the admission is officially submitted.The administrator of the college or other institute can manage the program information and students information, he can also manage( approve or reject ) the students' admissions.

There 2 kinds of users(roles) in this applications.

- Students
- The administrator of a college

## Business Rules

For the students:

- A student must register before he can submit a program application
- A student can only create one admission.
- Before the admission is submitted officially, the admission can be searched and updated. when it is submitted,the admission can't be updated again.the student can check it and get the admission's result: being approved or rejected.
- A student can update his/her own profile

For the administrators:

- The administrator can manage the programs information: adding, updating, Deleting
- The administrator can manage the students information: fetching, Deleting
- The administrator can manage the admissions information: fetching, Approving, Rejecting

The login and register button is on the upper-right corner of the landing page.after login, the user will see different interface and menus according to their roles : student interfaces or administrator interfaces.
   
## Technical Features

The Online Admission system is Based on following technical platforms and software to work properly:

- [JavaScript] - with Ajax and JQuery!
- [Mysql] - The Database
- [Bootstrap] - great UI boilerplate for modern web apps
- [Nodejs] - evented I/O for the backend
- [Express] - fast node.js network app framework 
- [Cors] - Cross-Origin Resource Sharing
- [JOI] - The most powerful schema description language and data validator for JavaScript.

## Installation

The Online Admission system requires [Node.js](https://nodejs.org/) v10+ to run.

1.Install the Mysql Database and initialize the admissions database.

```sh
execute the query statements  inschema.sql in the root directory
```

2.For the backend application, install the dependencies and devDependencies and start the server.

```sh
cd admission
npm i
nodemon
```

3.For the frontend applacation...

```sh
npm install --save http-server
http-server -c-l
```

## Run the application

1.The home page

- open http://127.0.0.1:8080/ in your default browser

- [HomePage](https://bitbucket.org/johnabbott-ipd23/jiandong-han/src/master/mdpictures/index.png)

2.For a student

- register a new student
- [Register Page](https://bitbucket.org/johnabbott-ipd23/jiandong-han/src/master/mdpictures/registration.png)
- after registration,you will enter the admission create and submit page, the loginned user name is displayed in the upper-right corner of page 
- [Create a admission](https://bitbucket.org/johnabbott-ipd23/jiandong-han/src/master/mdpictures/createanew%20admission.png)
- you can choose a program and create a new admission.the program information is displayed in the student pages and you can refer to them.
- [Update Profile](https://bitbucket.org/johnabbott-ipd23/jiandong-han/src/master/mdpictures/updateprofile.png)
- you can update you admission and choose different program before submitting it officially
- Submit the admission and waiting for the results: Approved or Rejected 

2.For a administrator

- login as a administrator (with username: admin@admin.com and password: 1234 )
- Managing the admission information. In this admissions_management page, you will get all admission lists automatically and you can click on one of them, then you will see the edit panel. in the edit pane you can choose to approve or reject it(change its status).
- [Admission Management](https://bitbucket.org/johnabbott-ipd23/jiandong-han/src/master/mdpictures/admissionmanagement.png)
- Managing the program information. In this programs_management page, you will get all program lists automatically and you can click on one of them, then you will see the edit panel. in the edit pane you can update it(change the  program description). you can also add new program or delete a existing program with related buttons in this page.
- [Program Management](https://bitbucket.org/johnabbott-ipd23/jiandong-han/src/master/mdpictures/programmanagement.png)
- Managing the students information. In this students_management page, you will get all student lists automatically and you can click on one of them, then you will see the edit panel(readonly).
- [Student Management](https://bitbucket.org/johnabbott-ipd23/jiandong-han/src/master/mdpictures/studentmanagement.png)

