'use strict';

const mysql = require('mysql');

//local mysql db connection
const connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'password',
    database : 'admissionsdb'
});

connection.connect(function(err) {
    if (err) {
        console.log("DB dieeeddddd!!! Save the world!!");
        throw err;
    }
    else {
        console.log("DB connection successful");
    }
    
});

module.exports = connection;