const express = require('express');
const router = express.Router();
const sql = require('../db');
const Joi=require('joi');
const validator =require('express-joi-validation').createValidator({});

// The declaration if createadmissionSchema used in POST and PUT API 
const createadmissionSchema = Joi.object({
  student_id: Joi.number().required(),
  program_id: Joi.number().required(),
  status: Joi.string().max(20).required()
});

// The declaration if createadmissionSchema used in PATCH API 
const patchadmissionSchema = Joi.object({
  status: Joi.string().max(20).required()
});

/* GET API */
/* Get all admissions listing */
router.get('/', function(req, res) {

  sql.query("SELECT * FROM admissions", (error, results, fields) => {
    if(error) throw error;

    res.json(results);

  });

});

/* GET API */
/* Get all admissions listing */
router.get('/withpname/', function(req, res) {

  sql.query("SELECT a.id, a.admission_date, a.status, p.id as programid, p.name as programname, p.description, u.id as userid, u.name as username, u.email FROM admissions as a join programs as p on a.program_id = p.id join users as u on a.student_id = u.id", (error, results, fields) => {
    if(error) throw error;

    res.json(results);

  });

});

/* GET API */
/* Get get one admission by id */
router.get('/:id', function(req, res) {

  const admissionid = req.params.id;

  sql.query("SELECT * FROM admissions WHERE id = ? ", admissionid, (error, results, fields) => {
    if(error) throw error;

    res.json(results);

  });

});

/* GET API */
/* Get get admission information by student_id */
router.get('/bystudentid/:studentid', function(req, res) {

  const studentid = req.params.studentid;

  sql.query("SELECT * FROM admissions WHERE student_id = ? ", studentid, (error, results, fields) => {
    if(error) throw error;

    res.json(results);

  });

});

/* POST API */
/* Creating a new admission */
router.post('/', validator.body(createadmissionSchema), (req,res) => {
  console.log(req.body);
  const sqlString = `INSERT INTO admissions (student_id, program_id, status) VALUES ('${req.body.student_id}', '${req.body.program_id}', '${req.body.status}')`;
  
  sql.query(sqlString, (error, results) => {
    if (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        res.status(400).send({
          message: "User already exists. Please sign in or use another email."
        });
      }
      next(error);
      return;
      }

    res.json(results);

  });

});

/* PUT API */
/* update one admission by id */
router.put('/:id',  validator.body(createadmissionSchema), function(req, res) {

  console.log(req.params);
  console.log(req.boday);
  const data = req.body;
  const admissionid = req.params.id;

  sql.query('UPDATE admissions SET student_id = ?, program_id = ?, status = ? where id = ?', [data.student_id, data.program_id, data.status, admissionid], function(error, results, fields) {
    if (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        res.status(400).send({
          message: "User already exists. Please sign in or use another email."
        });
      }
      next(error);
      return;
      }

    res.json(results);
  
  });

});

/* PATCH API */
/* update one admission's status by id */
router.patch('/:id',  validator.body(patchadmissionSchema), function(req, res) {

  console.log(req.params);
  const data = req.body;
  const admissionid = req.params.id;

  sql.query('UPDATE admissions SET status=? where id = ?', [data.status, admissionid], function(error, results, fields) {
    if (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        res.status(400).send({
          message: "User already exists. Please sign in or use another email."
        });
      }
      next(error);
      return;
      }

    res.json(results);
  
  });

});

/* DELETE API */
/* Deleting a admission record by id */
router.delete('/:id', function(req, res) {

  console.log(req.params);
  const admissionid = req.params.id;
  
  sql.query('DELETE FROM admissions where id = ?', admissionid, function(error, results, fields) {
      if (error) throw error;

      res.json(results);

    });

});

module.exports = router;


