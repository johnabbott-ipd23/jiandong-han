const express = require('express');
const router = express.Router();
const sql = require('../db');

const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({});

// The declaration if createprogramSchema used in PUT API 
const createprogramSchema = Joi.object({
  name: Joi.string().min(3).max(100).required(),
  description: Joi.string().min(3).max(500).required()
});

/* GET API */
/* Get all programs listing */
router.get('/', function(req, res) {

  sql.query("SELECT * FROM programs", (error, results, fields) => {
    if(error) throw error;

    res.json(results);

  });

});

/* GET API */
/* Get get one program by id */
router.get('/:id', function(req, res) {
  console.log(req.body);
  console.log(req.params);
  const programid = req.params.id;

  sql.query("SELECT * FROM programs WHERE id = ? ", programid, (error, results, fields) => {
    if(error) throw error;

    res.json(results);

  });

});

// The declaration if createprogramSchema used in POST API
const validationSchema = Joi.object({
  name: Joi.string().min(3).max(100).required(),
  description: Joi.string().min(3).max(500).required()
});

/* POST API */
/* Creating a new program */
router.post('/', validator.body(validationSchema), (req,res) => {
  console.log(req.body);
  const sqlString = `INSERT INTO programs (name, description) VALUES ('${req.body.name}', '${req.body.description}')`;
  
  sql.query(sqlString, (error, results) => {
    if (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        res.status(400).send({
          message: "User already exists. Please sign in or use another email."
        });
      }
      next(error);
      return;
      }

    res.json(results);

  });

});

/* PUT API */
/* update one program by id */
router.put('/:id',  validator.body(createprogramSchema), function(req, res) {

  console.log(req.params);
  const data = req.body;
  const programid = req.params.id;

  sql.query('UPDATE programs SET name=?, description=? where id = ?', [data.name, data.description, programid], function(error, results, fields) {
    if (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        res.status(400).send({
          message: "User already exists. Please sign in or use another email."
        });
      }
      next(error);
      return;
      }

    res.json(results);
  
  });

});

/* DELETE API */
/* Deleting a program record by id */
router.delete('/:id',  validator.body(createprogramSchema), function(req, res) {

  console.log(req.params);
  const programid = req.params.id;
  
  sql.query('DELETE FROM programs where id = ?', programid, function(error, results, fields) {
      if (error) throw error;

      res.json(results);

    });

});

module.exports = router;


