const express = require('express');
const router = express.Router();
const sql = require('../db');
const Joi=require('joi');
const validator =require('express-joi-validation').createValidator({});

// The declaration if createuserSchema used in POST and PUT API 
const createuserSchema = Joi.object({
  name: Joi.string().min(3).max(100).required(),
  email: Joi.string().min(3).max(100).required(),
  address: Joi.string().min(3).max(100).required(),
  zipcode: Joi.string().min(3).max(7).required(),
  telephone: Joi.string().min(3).max(15).required(),
  password: Joi.string().min(3).max(15).required(),
});

/* GET API */
/* Get all users(students) listing */
router.get('/', function(req, res) {

  sql.query("SELECT * FROM users where type = 'student'", (error, results, fields) => {
    if(error) throw error;

    res.json(results);

  });

});

/* GET API */
/* Get get one user(student) by id */
router.get('/:id', function(req, res) {

  const userid = req.params.id;

  sql.query("SELECT * FROM users WHERE id = ? ", userid, (error, results, fields) => {
    if(error) throw error;

    res.json(results);

  });

});

/* GET API */
/* Get get one user(student) by email */
router.get('/email/:email', function(req, res) {

  console.log(req.params);
  const email = req.params.email;

  sql.query("SELECT * FROM users WHERE email = ? ", email, (error, results, fields) => {
    if(error) throw error;

    res.json(results);

  });

});

/* GET API */
/* Get get one user by email and password, used for login validation */
router.get('/login/:email/:password', function(req, res) {

  console.log(req.params);

  sql.query("SELECT * FROM users WHERE email = ? and password = ?", [req.params.email, req.params.password], (error, results, fields) => {
    if(error) throw error;

    res.json(results);

  });

});

/* POST API */
/* Creating a new user(student) */
router.post('/', validator.body(createuserSchema), (req,res) => {
  
  const sqlString = `INSERT INTO users (name, email, address, zipcode, telephone, password, type) VALUES 
                    ('${req.body.name}', '${req.body.email}', '${req.body.address}', '${req.body.zipcode}', '${req.body.telephone}', '${req.body.password}', 'student')`;
  
  sql.query(sqlString, (error, results) => {
    if (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        res.status(400).send({
          message: "User already exists. Please sign in or use another email."
        });
      }
      next(error);
      return;
      }

    res.json(results);

  });

});

/* PUT API */
/* update one user(student) by id */
router.put('/:id',  validator.body(createuserSchema), function(req, res) {

  console.log(req.params);
  console.log(req.body);
  const data = req.body;
  const userid = req.params.id;

  sql.query('UPDATE users SET name=?, email=?, address=?, zipcode=?, telephone=?, password=? where id = ?', 
            [data.name, data.email, data.address, data.zipcode, data.telephone, data.password, userid], function(error, results, fields) {
    if (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        res.status(400).send({
          message: "User already exists. Please sign in or use another email."
        });
      }
      next(error);
      return;
      }

    res.json(results);
  
  });

});

/* DELETE API */
/* Deleting a user(student) record by id */
router.delete('/:id', function(req, res) {

  console.log(req.params);
  const userid = req.params.id;
  
  sql.query('DELETE FROM users where id = ?', userid, function(error, results, fields) {
      if (error) throw error;

      res.json(results);

    });

});

module.exports = router;


