CREATE DATABASE admissionsdb;
USE admissionsdb;

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL UNIQUE,
  `address` varchar(100) NOT NULL,
  `zipcode` varchar(7) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `password` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL    
);

CREATE TABLE IF NOT EXISTS `programs` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(100) NOT NULL UNIQUE,
  `description` varchar(500) NOT NULL
);

CREATE TABLE IF NOT EXISTS `admissions` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `admission_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(20) NOT NULL  
);

ALTER TABLE `admissions`
	ADD CONSTRAINT fk_admissions_users FOREIGN KEY(student_id) REFERENCES users(id);

ALTER TABLE `admissions`
	ADD CONSTRAINT fk_admissions_programs FOREIGN KEY(program_id) REFERENCES programs(id);
    
INSERT INTO `programs` (`id`, `name`, `description`) VALUES
(1, 'Cisco Networking', 'To teach students to design, build and maintain computer networks. Upon successful completion of the four courses, students are eligible to write the Cisco Certified Network Associate (CCNA) examination'),
(2, 'Full Stack Developer', 'The Full-Stack Developer program is a full-time credit program that leads to an Attestation of College Studies (A.E.C.). The objective of the Full-Stack Developer (FSD) program is to provide hands-on, up-to-date training using current industry-standard methodologies and technologies.'),
(3, 'Mobile Application Development', 'This program, leading to the Attestation d’études collégiales, provides specialized training to meet the increasing needs for labour in this sector. Students will receive hands-on training in the development of mobile apps, establishing the foundation necessary to develop native and web-based apps, both on the iOS and Android mobile platforms.');    

INSERT INTO `users` (`id`, `name`, `email`, `address`, `zipcode`, `telephone`, `password`, `type`) VALUES
(1, 'Admin', 'admin@admin.com', '12,boul eva street', 'H9J 1B5', '5144443333', '1234', 'admin'),
(2, 'Eva Brad', 'eva@eva.com', '12,boul eva street', 'H9J 1B5', '5144443333', '1234', 'student'),
(3, 'Tom Jerry', 'tom@tom.com', '1122,boul dorval street', 'H3C 6A6', '5148887777', '1234', 'student'),
(4, 'John Smith', 'john@john.com', '22,boul john street', 'H9V 3A2', '5146667777', '1234', 'student');

INSERT INTO `admissions` (`id`, `student_id`, `program_id`, `status`) VALUES
(1, 2, 2, 'Saved'),
(2, 3, 3, 'Submitted');

SELECT * from programs;

SELECT * from users;

SELECT * from admissions;